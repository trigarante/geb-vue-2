@extends('layouts.gebLayout')
@section('title')
 Pre Candidato
@endsection
@section('content')
<div id="app">
	<pre-candidato-update-component :precandidato = "{{$idPrecandidato}}"></pre-candidato-update-component>
</div>
@endsection