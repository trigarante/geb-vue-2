@extends('layouts.gebLayout')
@section('title')
Crear Precandidato
@endsection
@section('content')
<div id="app">
<pre-candidato-view-component :solicitud = "{{$idSolicitud}}"></pre-candidato-view-component>
</div>
@endsection