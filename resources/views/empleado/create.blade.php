@extends('layouts.gebLayout')
@section('title')
Crear Empleado
@endsection
@section('content')
<div id="app">

	<empleado-create-component :seguimiento = "{{$idSeguimiento}}"></empleado-create-component>
</div>
@endsection