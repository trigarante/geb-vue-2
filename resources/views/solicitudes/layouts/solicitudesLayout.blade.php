<!DOCTYPE html>
<html>
<head>
  <title>@yield('title')</title>
  <link rel="stylesheet" href="{{asset('css/app.css')}}" async>
   <meta name="csrf-token" content="{{ csrf_token() }}">
  @yield('styles')
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">GEB</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="/solicitud">Ver Solicitudes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/preCandidato">Ver Pre Candidatos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/candidato">Ver Candidatos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/seguimiento">Ver Seguimiento</a>
      </li>
    </ul>
  </div>
</nav>
  @yield('content')
</body>
<script src="{{asset('js/app.js')}}"></script>
@yield('script')
</html>