@extends('layouts.gebLayout')
@section('title')
Candidato
@endsection
@section('content')
<div id="app">
	<candidato-view-component :precandidato = "{{$idPrecandidato}}"></candidato-view-component>
</div>
@endsection