@extends('layouts.gebLayout')
@section('title')
Candidato
@endsection
@section('content')
<div id="app">

	<candidato-create-component :precandidato = "{{$idPreCandidato}}"></candidato-create-component>
</div>
@endsection