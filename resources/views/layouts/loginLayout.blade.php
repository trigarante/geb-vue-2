<!DOCTYPE html>
<html>
<head>
  <title>@yield('title')</title>
  <link rel="stylesheet" href="{{asset('css/app.css')}}" async>
   <meta name="csrf-token" content="{{ csrf_token() }}">
  @yield('styles')
</head>
<body>
  @yield('content')
</body>
<script src="{{asset('js/app.js')}}"></script>
@yield('script')
</html>