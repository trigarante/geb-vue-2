<!DOCTYPE html>
<html>
<head>
  <title>@yield('title')</title>
  <link rel="stylesheet" href="{{asset('css/app.css')}}" async>
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
  @yield('styles')
</head>
<body>
<div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>GEB</h3>
            </div>

            <ul class="list-unstyled components">
                <li>
                    <a href="/solicitudCreate">Crear Solicitud</a>
                </li>

                <li>
                    <a href="/solicitud">Solicitudes</a>
                </li>
                
                <li>
                    <a href="/preCandidato">Pre Candidatos</a>
                </li>
                <li>
                    <a href="/candidato">Candidatos</a>
                </li>
                <li>
                    <a href="/seguimiento">Seguimiento</a>
                </li>
                <li>
                    <a href="/empleado">Empleados</a>
                </li>
                <li>
                    <a href="/catalogos">Catalogos</a>
                </li>
            </ul>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Ocultar</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                
                </div>
            </nav>

         @yield('content')  
    </div>
  
</body>
<script src="{{asset('js/app.js')}}"></script>
<script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
@yield('script')
</html>