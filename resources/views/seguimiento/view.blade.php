@extends('layouts.gebLayout')
@section('title')
Seguimiento de Candidatos
@endsection
@section('content')
<div id="app">
<seguimiento-view-component :candidato = "{{$idCandidato}}"> </seguimiento-view-component>
</div>
@endsection