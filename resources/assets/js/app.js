
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
 import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);
Vue.component('pre-candidato-index-component', require('./components/precandidato/PreCandidatoIndex.vue'));
Vue.component('pre-candidato-update-component', require('./components/precandidato/PreCandidatoUpdate.vue'));
Vue.component('pre-candidato-create-component', require('./components/precandidato/PreCandidatoCreate.vue'));
Vue.component('pre-candidato-view-component', require('./components/precandidato/PreCandidatoView.vue'));
Vue.component('solicitudes-index-component', require('./components/solicitudes/SolicitudesIndex.vue'));
Vue.component('solicitudes-create-component', require('./components/solicitudes/SolicitudesCreate.vue'));
Vue.component('candidato-index-component', require('./components/candidato/CandidatoIndex.vue'));
Vue.component('candidato-create-component', require('./components/candidato/CandidatoCreate.vue'));
Vue.component('candidato-view-component', require('./components/candidato/CandidatoView.vue'));
Vue.component('reporte-candidato-component', require('./components/reportes/ReporteCandidato.vue'));
Vue.component('seguimiento-create-component', require('./components/seguimiento/SeguimientoCreate.vue'));
Vue.component('seguimiento-index-component', require('./components/seguimiento/SeguimientoIndex.vue'));
Vue.component('seguimiento-view-component', require('./components/seguimiento/SeguimientoView.vue'));
Vue.component('empleado-create-component', require('./components/empleado/EmpleadoCreate.vue'));
Vue.component('empleado-index-component', require('./components/empleado/EmpleadoIndex.vue'));
Vue.component('catalogos-index-component', require('./components/catalogos/CatalogosIndex.vue'));
Vue.component('login-component', require('./components/Login.vue'));

const app = new Vue({
    el: '#app'
});
