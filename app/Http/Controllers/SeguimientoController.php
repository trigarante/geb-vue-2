<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SeguimientoController extends Controller
{
    public function seguimientoCreate(Request $request)
    {
    	$id = $request->id;
    	return view('seguimiento.create')->with(['idCandidato'=>$id]);
    }

    public function seguimientoView(Request $request)
    {
    	$id = $request->id;
    	return view('seguimiento.view')->with(['idCandidato'=>$id]);
    }
}
