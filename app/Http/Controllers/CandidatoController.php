<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CandidatoController extends Controller
{
    public function crearCandidato(Request $request)
    {
    	$id = $request->id;
    	return view('candidato.create')->with('idPreCandidato', $id);
    }

    public function verCandidato(Request $request)
    {
    	$id = $request->id;
    	return view('candidato.view')->with('idPrecandidato', $id);
    }
}
