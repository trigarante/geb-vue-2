<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class PreCandidatoController extends Controller
{
    public function idSolicitud(Request $request)
    {
    	$idSolicitud = $request->id;
    	return view('precandidato.create')->with(['idSolicitud'=>$idSolicitud]);
    }

    public function idPrecandidato(Request $request)
    {
    	$id = $request->id;
    	return view('precandidato.update')->with('idPrecandidato', $id);
    }

    public function verPreCandidato(Request $request)
    {       
        $id = $request->id;
        return view('precandidato.view')->with('idSolicitud', $id);
    }


    public function create()
    {
        return view('precandidato.create')->with(['idSolicitud'=>$this->idSolicitud,'foto'=>$this->foto]);
    }

    public function saveFoto(Request $request)
    {
        $imagen = $request->imagen;
        $nombreImagenGuardada =$request->nombre_imagen;
        $imagenDecodificada = base64_decode($imagen);
        file_put_contents($nombreImagenGuardada, $imagenDecodificada);
    }
}