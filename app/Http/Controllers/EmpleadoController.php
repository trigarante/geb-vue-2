<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmpleadoController extends Controller
{
    public function empleadoCreate(Request $request)
    {
    	$id = $request->id;
    	return view('empleado.create')->with('idSeguimiento',$id);
    }
}
