<?php

Route::get('/', function () {
    return view('solicitudes.create');
});
// precandidatos
Route::get('/preCandidato',function(){
	return view('precandidato.index');
});
Route::get('/preCandidatoUpdate',function(){
	return view('precandidato.update');
});
Route::get('/preCandidatoCreate','PreCandidatoController@create')->name('preCandidatoCreate');
// solicitudes
Route::get('/solicitud',function(){
	return view('solicitudes.index');
});
Route::get('/solicitudCreate',function(){
	return view('solicitudes.create');
});
Route::get('/candidato',function(){
	return view('candidato.index');
});
Route::get('/reporteCandidato',function(){
	return view('reportes.candidato');
});
Route::get('/seguimiento',function(){
	return view('seguimiento.index');
});
Route::get('/empleado',function(){
	return view('empleado.index');
});
Route::get('/catalogos',function(){
	return view('catalogos.catalogosIndex');
});


Route::post('/idSolicitud','PreCandidatoController@idSolicitud');

Route::post('/preCandidatoUpdate','PreCandidatoController@idPrecandidato');

Route::post('/verPreCandidato','PreCandidatoController@verPreCandidato');

Route::post('/crearCandidato','CandidatoController@crearCandidato');

Route::post('/verCandidato','CandidatoController@verCandidato');

Route::post('/seguimientoCreate','SeguimientoController@seguimientoCreate');

Route::post('/empleadoCreate','EmpleadoController@empleadoCreate');

Route::post('/seguimientoView','SeguimientoController@seguimientoView');

Route::get('/fotoPrecandidato',function()
{
	return view('precandidato.foto');
});

Route::post('/saveFoto','PreCandidatoController@saveFoto');

Route::post('/userLogin','LoginController@LoginUser');